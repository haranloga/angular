import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColsectionComponent } from './colsection.component';

describe('ColsectionComponent', () => {
  let component: ColsectionComponent;
  let fixture: ComponentFixture<ColsectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColsectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColsectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
