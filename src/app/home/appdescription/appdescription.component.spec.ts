import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppdescriptionComponent } from './appdescription.component';

describe('AppdescriptionComponent', () => {
  let component: AppdescriptionComponent;
  let fixture: ComponentFixture<AppdescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppdescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppdescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
