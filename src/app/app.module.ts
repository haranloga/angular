

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule  } from '@angular/forms';
// tslint:disable-next-line:import-spacing
import { HttpModule  } from  '@angular/http';

import { RouterModule} from '@angular/router';
// , Routes
import { appRoutes } from './app.routes';


import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterpageComponent } from './registerpage/registerpage.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { ValidatepageComponent } from './validatepage/validatepage.component';
import { NotfoundpageComponent } from './notfoundpage/notfoundpage.component';

import {FlashMessagesModule} from 'angular2-flash-messages';
import {FlashMessagesService} from 'angular2-flash-messages';

import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AuthService} from './auth.service';

import {environment} from '../environments/environment';

import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FarmerComponent } from './farmer/farmer.component';
import { FarmerListComponent } from './farmer-list/farmer-list.component';
import { FarmerService } from './shared/farmer.service';


import { ImagesliderComponent } from './home/imageslider/imageslider.component';
import { AppdescriptionComponent } from './home/appdescription/appdescription.component';
import { ColsectionComponent } from './home/colsection/colsection.component';
import { AddsComponent } from './home/adds/adds.component';
import { ConnectComponent } from './home/connect/connect.component';
import { FooterComponent } from './home/footer/footer.component';





// const appRoutes: Routes = [
//    {path: 'home' , component: HomeComponent},
//    {path: 'login' , component: LoginpageComponent},
//    {path: 'register' , component: RegisterpageComponent},
//    {path: 'validate' , component: ValidatepageComponent},
//    {path: '**' , component: HomeComponent }
//  ];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    RegisterpageComponent,
    LoginpageComponent,
    ValidatepageComponent,
    NotfoundpageComponent,
    FarmerComponent,
    FarmerListComponent,
    ImagesliderComponent,
    AppdescriptionComponent,
    ColsectionComponent,
    AddsComponent,
    ConnectComponent,
    FooterComponent


  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpModule,
 RouterModule.forRoot(appRoutes),
 AngularFireAuthModule,
 AngularFireModule.initializeApp(environment.firebaseConfig),
 FlashMessagesModule,
 ReactiveFormsModule,
 AngularFireDatabaseModule

  ],
  providers: [AuthService, FlashMessagesService,FarmerService],
  bootstrap: [AppComponent,ImagesliderComponent,AppdescriptionComponent,
    ColsectionComponent,AddsComponent,ConnectComponent,FooterComponent]
})
export class AppModule { }
