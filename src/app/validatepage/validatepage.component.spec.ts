import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidatepageComponent } from './validatepage.component';

describe('ValidatepageComponent', () => {
  let component: ValidatepageComponent;
  let fixture: ComponentFixture<ValidatepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidatepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidatepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
