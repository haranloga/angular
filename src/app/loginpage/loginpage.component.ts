
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
// tslint:disable-next-line:import-spacing
import { Router }  from  '@angular/router';
import { FlashMessagesService, FlashMessagesModule } from 'angular2-flash-messages';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
public email: string;
public password: string;


  constructor(
    public authService: AuthService,
    public router: Router,
    public flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onSubmitLogin() {
 this.authService.loginEmail(this.email, this.password)
 .then ((res) => {
   this.router.navigate(['/home']);
   this.flashMessage.show('Logged-In', {cssClass: 'alert-success', timeout: 4000});
 }).catch ((err) => {
console.log(err);
this.flashMessage.show(err.message, {cssClass: 'alert-danger', timeout: 4000});
this.router.navigate(['/login']);
 });
  }

  onClickGoogleLogin() {
    this.authService.loginGoogle()
    .then((res) => {
      this.router.navigate(['/home']);
    }).catch(err => console.log(err.message));
  }

  onClickFacebookLogin() {
    this.authService.loginFacebook()
    .then((res) => {
      this.router.navigate(['/home']);
    }).catch(err => console.log(err.message));
  }
}

