
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router   } from '@angular/router';
import { FlashMessagesService, FlashMessagesModule } from 'angular2-flash-messages';
@Component({
  selector: 'app-registerpage',
  templateUrl: './registerpage.component.html',
  styleUrls: ['./registerpage.component.css']
})
export class RegisterpageComponent implements OnInit {
public email: string;
public password: string;

  constructor(public authService: AuthService,
    public router: Router,
    public flashMessage: FlashMessagesService
  ) { }


  ngOnInit() {
  }
  onSubmitAddUser() {
this.authService.registerUser(this.email, this.password)
.then((res) => {
  this.flashMessage.show('Registered successfully', {cssClass: 'alert-success', timeout: 4000});
  this.router.navigate(['/farmer']);
//  console.log('logged In');
//  console.log(res);
}).catch((err) => {
  this.flashMessage.show(err.message, {cssClass: 'alert-danger', timeout: 4000});
console.log(err);
});
}
}
