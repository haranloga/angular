import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FarmerComponent } from './farmer/farmer.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { RegisterpageComponent } from './registerpage/registerpage.component';
import { ValidatepageComponent } from './validatepage/validatepage.component';
import { FarmerListComponent } from './farmer-list/farmer-list.component';



export const appRoutes: Routes = [
{
    path: '',
    component: HomeComponent
},

{
    path: 'login' , 
    component: LoginpageComponent
},

{
    path: 'register' , 
    component: RegisterpageComponent
},

{
    path: 'validate' , 
    component: ValidatepageComponent
},

{
    path: 'home' , 
    component: HomeComponent 
},

{
    path: 'farmer',
    component: FarmerComponent
},

{
    path: 'farmer-list',
    component: FarmerListComponent
}

];
// const appRoutes: Routes = [
//     {path: 'home' , component: HomeComponent},
//     {path: 'login' , component: LoginpageComponent},
//     {path: 'register' , component: RegisterpageComponent},
//     {path: 'validate' , component: ValidatepageComponent},
//     {path: '**' , component: HomeComponent }
//   ];